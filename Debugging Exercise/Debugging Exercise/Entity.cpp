#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Entity.h"

Entity::Entity()
{
	health = 100;
}


Entity::~Entity()
{
}

int Entity::attack()
{
	return 0;
}

void Entity::takeDamage(int damage)
{
	health -= damage;
	if (health < 0)
		health = 0;
}

bool Entity::isAlive()
{
	return health > 0;
}

int Entity::getHealth()
{
	return health;
}

int Entity::getMaxHealth()
{
	return maxHealth;
}
