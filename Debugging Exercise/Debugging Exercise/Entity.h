#pragma once
class Entity
{
public:
	Entity();
	~Entity();

	virtual int attack();
	virtual void takeDamage(int damage);
	bool isAlive();
	int getHealth();
	int getMaxHealth();

protected:
	int health = 100;
	int maxHealth = 100;
};

