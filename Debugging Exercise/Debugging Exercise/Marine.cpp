#include <iostream>
#include "Marine.h"

Marine::Marine()
{
}


Marine::~Marine()
{
}

int Marine::attack()
{
	int minroll = 20;
	int maxroll = 50;

	int randomnumber = std::rand() % (maxroll + 1) + minroll;

	return randomnumber;
}

void Marine::takeDamage(int damage)
{
	health -= damage;
	if (health < 0)
		health = 0;
}
