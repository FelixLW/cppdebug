// A Debugging Exercise by Marc Chee (marcc@aie.edu.au)
// 18/12/2016

// Marines are trying to "de-bug" an area (haha . . . haha)
// However something's up with this code . . . figure out what's going wrong
// When you're finished, there should be no compiler errors or warnings
// The encounter should also run and finish correctly as per the rules in the comments

// In many cases there are differences between what's in otherwise identical sections
// for Marines and Zerglings. It's good to be able to tell what the differences are
// and why they might be important.

// What's efficient and inefficient? Why?
// What uses more memory and what doesn't? Why?

#include <iostream>
#include <vector>
#include "Marine.h"
#include "Zergling.h"

using std::vector;
using std::cout;
using std::endl;

// Is there a Marine Alive?
bool marineAlive(vector<Marine> squad)
{
	return squad.size() > 0;
}

// Is there a zergling Alive
bool zerglingAlive(vector<Zergling> swarm)
{
	return swarm.size() > 0;
}

int main()
{
	vector<Marine> squad;
	vector<Zergling> swarm;

	int squadSize = 10;
	int swarmSize = 20;

	// Set up the Squad and the Swarm at their initial sizes listed above
	for (size_t i = 0; i < squadSize; i++)
	{
		Marine m;
		squad.push_back(m);
	}

	for (size_t i = 0; i < swarmSize; i++)
	{
		Zergling z;
		swarm.push_back(z);
	}

	cout << "A squad of marines approaches a swarm of Zerglings and opens fire! The Zerglings charge!" << endl << endl;

	// Attack each other until only one team is left alive
	while (marineAlive(squad) || zerglingAlive(swarm)) // If anyone is left alive to fight . . .
	{
		if (marineAlive(squad)) // if there's at least one marine alive
		{
			for (size_t i = 0; i < squad.size(); i++) // go through the squad
			{
				// if there is an enemy to attack
				if (zerglingAlive(swarm))
				{
					// each marine will attack the first zergling in the swarm
					cout << "A marine fires for " << squad[i].attack() << " damage. " << endl;
					int damage = squad[i].attack();
					swarm[0].takeDamage(damage);
					cout << "Zergling health is now " << swarm[0].getHealth() << "/" << swarm[0].getMaxHealth() << endl << endl;
					if (!swarm[0].isAlive()) // if the zergling dies, it is removed from the swarm
					{
						cout << "The zergling target dies" << endl << endl;
						swarm.erase(swarm.begin());
						cout << squad.size() << "/" << squadSize << " Marine(s) alive!" << endl;
						cout << swarm.size() << "/" << swarmSize << " Zergling(s) alive!" << endl << endl;
					}
				}
				else
				{
					break;
				}
			}
		}

		if (zerglingAlive(swarm)) // if there's at least one zergling alive
		{
			for (size_t i = 0; i < swarm.size(); i++) // go through the squad
			{
				// if there is an enemy to attack
				if (marineAlive(squad))
				{
					// each zergling will attack the first marine in the squad
					cout << "A Zergling attacks for " << swarm[i].attack() << " damage. " << endl;
					int damage = swarm[i].attack();
					squad[0].takeDamage(damage);
					cout << "Marine health is now " << squad[0].getHealth() << "/" << squad[0].getMaxHealth() << endl << endl;
					if (!squad[0].isAlive()) // if the marine dies, it is removed from the squad
					{
						cout << "The marine dies" << endl << endl;
						squad.erase(squad.begin());
						cout << squad.size() << "/" << squadSize << " Marine(s) alive!" << endl;
						cout << swarm.size() << "/" << swarmSize << " Zergling(s) alive!" << endl << endl;
					}
				}	
				else
				{
					break;
				}
			}
		}

		// Once one team is completely eliminated, the fight ends and one team wins
		
		if (squad.size() > 1 && swarm.size() < 1)
		{
			cout << "The fight is over. ";
			cout << "The Marines win." << endl;
			break;
		}
		else if (swarm.size() > 1 && squad.size() < 1)
		{
			cout << "The fight is over. ";
			cout << "The Zerg win." << endl;
			break;
		}
	}
}


