#include <iostream>
#include "Zergling.h"

Zergling::Zergling()
{
}


Zergling::~Zergling()
{
}

int Zergling::attack()
{
	int minroll = 5;
	int maxroll = 10;

	int randomnumber = std::rand() % (maxroll + 1) + minroll;

	return randomnumber;
}

void Zergling::takeDamage(int damage)
{
	health -= damage;
	if (health < 0)
		health = 0;
}
